drop database if exists todo;
create database todo;
use todo;

create table task (
    id int primary key auto_increment,
    description varchar(225) not null,
    done boolean default false,
    added timestamp default current_timestamp
);