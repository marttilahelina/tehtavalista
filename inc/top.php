<?php
$db = null;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <title>Todo</title>
</head>
<body>
    <div id="content">
    <?php
    try {
       $db = new PDO('mysql:host=localhost; dbname=todo; charset=utf8', 'root', '');
       $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOException $pdoex) {
       print "<p>Tietokannan avaus epäonnistui. " . $pdoex->getMessage() . "</p>";
    }
    ?>